# ParadigmsFinal
Names: Philip Baumann, Conor Nailos, Justin Garrard, Jacob Knutson

FOR INSTRUCTIONS FOR RUNNING GUI, SEE BOTTOM OF README

1. "Effectively selected data source"
For our project, we chose Kaggle's repository of wine reviews from Wine Magazine and WineEnthusiast. This CSV of data is 51 MB large and should provide us with a sufficient amount of data to produce reasonable results for our final project.

2. "Effectively create OO API + readme + tests"
As of Wednesday morning, we expect our OO API to resemble the following:

class _wine_database:
    
    init(self):
        self.wines = dict()
        self.tasters = dict()
        self.wineries = dict()
    
    load_wine(self)
    
    print_wine(self)
        print wines
    
    search_by_winery(self, winery)
        return wines
    
    search_by_taster(self, taster)
        return wines
    
    search_by_location(self, location)
        return wines
    
    get_wine(self, wine_id)
        return wine
    
    set_wine(self, wine_id, info) #info = [number, country, description, vineyard, rating (100), price, province, region_1, region_2, taster_name, taster_twitter, title, variety, winery]
    
    delete_wine(self, wine_id)
    
    get_country(self, wine_id)
        return country
    
    get_description(self, wine_id)
        return description
    
    get_vineyard(self, wine_id)
        return vineyard
    
    get_province(self, wine_id)
        return province
    
    get_taster(self, wine_id)
        return taster
    
    get_vintage(self, wine_id)
        return title
    
    get_variety(self, wine_id)
        return variety_of_grape
    
    get_highest_rated_wine(self)
        return wine
    
    search_for_wines_by_keyword(self, keyword)
        return wines
        
Our final version of the API was very similar to our initial thoughts. It had the following functions and objects:
    
    Objects:
        - wine = dict()
        - tasters = dict()
        - wineries = dict()
    
    Functions:
        - load_data
        - clear_data
        - print_wines
        - get_wine(wid)
        - get_wines
        - set_wine(wid, info)
        - delete_wine(wid)
        - get_taster(wid)
        - get_winery(wid)
        - get_highest_rated_wine

The reason we decreased the number of accessing functions was because we realized most of our data could simply be stored and accessed via a dictionary key/value system, rather than individual accessor functions. Our API focuses on data accessibility and should be used by the server to manipulate data for the client. In essence, this API should allow for custom wine recommendations based on user input. Additionally, this gives us a basic foundation so that we can manipulate our data as we please. 

Server API:

    PUT /wines/:key
        -Key = wine id number
        -payload structure {"title" : wineName, "country" : country, "winery" : winery, "variety" : red/white/..., "taster" : tasterName, "points" : wineScore, "description" : wineReview}
        -returns success
        
    POST /wines/
        -payload structure {"title" : wineName, "country" : country, "winery" : winery, "variety" : red/white/..., "taster" : tasterName, "points" : wineScore, "description" : wineReview}
        -returns wine id of new entry upon success -> response["id"]
    
    DELETE /wines/:key      (Not sure if we'd use this)
        -Key = wine id number
        -Deletes wine entry, and all taster and winery references to said wine id
        -returns success
    
    DELETE /wines/          (Not sure if we'd use this)
        -No key, deletes all data (wineries, taster, and winery)
        -returns success

    GET /wines/:key
        -Key = wine id number
        -returns {"result" : "success", "key" : key, "wine" : res['title'], "country" : res["country"], "winery" : res["winery"], "variety" : res["variety"], "taster" : res["taster"], "points" : res["points"], "description" : res["description"]}
        -result = error on failure
        
    GET /wines/
        -No key
        -Returns highest rated wine in our database -> {"result" : "success", "wid" : id}
        -This would be useful for a splash page -> "this is our best wine" kind of thing
    
    PUT /reset/
        -No key
        -Returns success
        -Reloads original winemag database
    
    GET /tasters/:key
        -Key = name of taster -> "Justin Garrard"
        -res["result"] = success or error
        -Returns list of wine ids that taster has reviewed -> res["keys"]
        -Idea would be user has a wine, likes that taster's reviews and wants to search for wine recommendations from that taster
        -Need to run a GET /wines/:key request for each wine id in the list that was returned
    
    GET /wineries/:key
        -Key = name of winery -> "Santa Alicia"
        -res["result"] = success or error
        -Returns list of wine ids that winery has made -> res["keys"]
        -Idea would be user has a wine and wants to search for highest rated wines that winery has made
        -Need to run a GET /wines/:key request for each wine id in the list that was returned
    
4. "Effectively build a web client that gets answers from your server"
    
    In order to run our entire project code:
    1. Make sure you are connected to student04.cse.nd.edu
    2. Start the server by typing '/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3 server.py'
    3. From a different terminal, type '/afs/nd.edu/user14/csesoft/2017-fall/python3.5/bin/python3 recommendqt.py' to run the gui
