import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4 import QtGui
from PyQt4 import QtCore
import requests
import json

class MoviesQT(QMainWindow):

    def __init__(self):
        super(MoviesQT, self).__init__()
        self.setWindowTitle("Wine Genius")
        self.central = MoviesCentral(parent = self)
        self.setCentralWidget(self.central)

        # file menu
        self.filemenu = self.menuBar().addMenu("File")
        fileExitAction = QAction("Exit", self)
        self.filemenu.addAction(fileExitAction)

        # review menu
        self.reviewmenu = self.menuBar().addMenu("Reviews")
        setTasterAction = QAction("Set Taster Name", self)
        self.reviewmenu.addAction(setTasterAction)
        newReviewAction = QAction("Add New Review", self)
        self.reviewmenu.addAction(newReviewAction)

        self.connect(fileExitAction, SIGNAL("triggered()"), self.exit_program)
        self.connect(setTasterAction, SIGNAL("triggered()"), self.set_taster)
        self.connect(newReviewAction, SIGNAL("triggered()"), self.new_review)

    def exit_program(self):
        app.quit()

    def set_taster(self):
        taster = QInputDialog.getText(self, 'Set Taster Name', 'Taster:')
        if (taster[1] == True):
            try:
                self.central.tasterName = str(taster[0])
    
                self.central.updateTaster()
            except:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setText("Error: input not valid")
                retval = msg.exec_()
        
    def new_review(self):
        if self.central.wid == -1: # wine has not been recommended
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error: no wine to recommend")
            retval = msg.exec_()
            return

        wineID = QInputDialog.getText(self, 'Add New Review', 'Score (out of 100):')
        if (wineID[1] == True):
            #check if int
            try:
                points = int(wineID[0])
                #{"title" : data["title"], "country" : data["country"], "winery" : data["winery"], 
                #"variety" : data["variety"], "taster" : data["taster"], "points" : data["points"], 
                # "description" : data["description"]}
                data_dict = {}
                data_dict["result"] = "success"
                data_dict["title"] = self.central.currentWine['wine']
                data_dict["taster"] = self.central.tasterName
                data_dict["points"] = points
                data_dict["country"] = self.central.currentWine['country']
                data_dict["description"] = self.central.currentWine['description']
                data_dict["variety"] = self.central.currentWine['variety']
                data_dict["winery"] = self.central.currentWine['winery']
                print(data_dict)
                r = requests.post(self.central.WINES_URL, data=json.dumps(data_dict))
            
            except Exception as e:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                print(e)
                msg.setText("Error: input not valid ")
                retval = msg.exec_()


class MoviesCentral(QWidget):
    def __init__(self, parent=None):
        super(MoviesCentral, self).__init__(parent)
        self.wid = 21001 #current wine id
        self.tasterName = "Michael Schachner"
        self.SITE_URL = 'http://student04.cse.nd.edu:52061'
        self.WINES_URL = self.SITE_URL + '/wines/'
        self.TASTER_URL = self.SITE_URL + '/tasters/'
        self.WINERY_URL = self.SITE_URL + '/wineries/'
        self.currentWine = requests.get(self.WINES_URL + str(self.wid)).json()
        # Taster name
        self.tasterLabel = QLabel("Taster: " + self.tasterName, self)
        self.tasterLabel.setAlignment(Qt.AlignCenter)

        # Wine name
        self.nameLabel = QLabel("Wine Name", self)
        self.nameLabel.setAlignment(Qt.AlignCenter)
        f = QtGui.QFont()
        f.setPointSize(20)
        f.setWeight(100)
        self.nameLabel.setFont(f)
        self.nameLabel.setVisible(False)

        # Image
        self.image = QImage('bottle.png')
        self.imageLabel = QLabel('no image')
        self.imageLabel.setAlignment(Qt.AlignCenter)
        self.imageLabel.setPixmap(QPixmap.fromImage(self.image))

        # Buttons
        self.recommendButton = QPushButton("Get Next Recommendation")
     
        # Dropdown
        self.comboBox = QtGui.QComboBox(self)
        self.comboBox.addItem("Winery")
        self.comboBox.addItem("Taster")

        # Textbox
        self.textbox = QLineEdit(self)
        self.textbox.resize(280, 40)

        # Wine info
        self.info = QLabel("Country: " + "Italy" + "\n" +
            "Winery: " + "Nicosia" + "\n" + 
            "Variety: " + "White Blend" + "\n" +
            "Points (out of 100): " + "100" + "\n" +
            "Description: " + "Aromas includ tropical fruit, broom, brimstone and dried herb. The palate isn't overly expressive, offering unriped apple, citrus and dried sage alongside brisk acidity." 
           
        )
        self.info.setWordWrap(True)
        self.info.setFixedWidth(250)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        self.info.setFont(font)
        self.info.setVisible(False)
    

	# horizontal
        hbox1 = QHBoxLayout()
        hbox1.addWidget(self.tasterLabel)
        hbox1.addStretch(1)
        hbox1.addStretch(1)

        hbox2 = QHBoxLayout()
        hbox2.addWidget(self.info)
        hbox2.addWidget(self.imageLabel)
        
        hbox3 = QHBoxLayout()
        hbox3.addStretch(1)
        hbox3.addStretch(1)
        hbox3.addStretch(1)

        hbox4 = QHBoxLayout()
        hbox4.addWidget(self.textbox)
        hbox4.addWidget(self.comboBox)

        hbox5 = QHBoxLayout()
        hbox5.addStretch(1)
        hbox5.addWidget(self.recommendButton)
        hbox5.addStretch(1)

        hbox6 = QHBoxLayout()
        hbox6.addStretch(1)
        hbox6.addWidget(self.nameLabel)
        hbox6.addStretch(1)

        hbox7 = QHBoxLayout()
        hbox7.addStretch(1)
        hbox7.addStretch(1)
        hbox7.addStretch(1)

        # vertical
        vbox = QVBoxLayout()
        vbox.addLayout(hbox4)
        vbox.addLayout(hbox5)
        vbox.addLayout(hbox7)
        vbox.addLayout(hbox6)
        vbox.addLayout(hbox2)
        vbox.addLayout(hbox3)
        vbox.addLayout(hbox1)
        self.setLayout(vbox)

        self.connect(self.recommendButton, SIGNAL("clicked()"), self.onRecommendPress)

    def update(self, key_ct):
        selection = str(self.comboBox.currentText())
        query = str(self.textbox.text())
        if selection == 'Winery':
            
            try:
                r = requests.get(self.WINERY_URL + str(query))
                r = r.json()
                if len(r['keys']) == 1:
                    new_wid = r['keys'][0][0]
                    r = requests.get(self.WINES_URL + str(new_wid))
                    self.currentWine = r.json()
                    self.nameLabel.setText(self.currentWine['wine'])
                    self.info.setText("Country: " + self.currentWine["country"] + "\nWinery: "+ self.currentWine["winery"] 
                    + "\nVariety: " + self.currentWine["variety"] + "\nPoints (out of 100): " + self.currentWine['points'] + "\nDescription: " + self.currentWine['description'])
                    self.info.setVisible(True)
                    self.nameLabel.setVisible(True)
                else:
                    new_wid = r['keys'][key_ct][0]
                    if self.currentWine['wine'] == requests.get(self.WINES_URL + str(new_wid)).json()['wine']:
                        key_ct = key_ct + 1
                        self.update(key_ct)
                    else:
                        new_wid = r['keys'][key_ct][0]
                        r = requests.get(self.WINES_URL + str(new_wid))
                        self.currentWine = r.json()
                        self.nameLabel.setText(self.currentWine['wine'])
                        self.info.setText("Country: " + self.currentWine["country"] + "\nWinery: "+ self.currentWine["winery"] 
                        + "\nVariety: " + self.currentWine["variety"] + "\nPoints (out of 100): " + self.currentWine['points'] + "\nDescription: " + self.currentWine['description'])
                        self.info.setVisible(True)
                        self.nameLabel.setVisible(True)
                        

            except:
                self.nameLabel.setText("Invalid Winery name, try again")
                self.nameLabel.setVisible(True)
        else:
            try:
                r = requests.get(self.TASTER_URL + str(query))
                r = r.json()
                if len(r['keys']) == 1:
                    new_wid = r['keys'][0][0]
                    r = requests.get(self.WINES_URL + str(new_wid))
                    self.currentWine = r.json()
                    self.nameLabel.setText(self.currentWine['wine'])
                    self.info.setText("Country: " + self.currentWine["country"] + "\nWinery: "+ self.currentWine["winery"] 
                    + "\nVariety: " + self.currentWine["variety"] + "\nPoints (out of 100): " + self.currentWine['points'] + "\nDescription: " + self.currentWine['description'])
                    self.info.setVisible(True)
                    self.nameLabel.setVisible(True)
                else:
                    new_wid = r['keys'][key_ct][0]
                    if self.currentWine['wine'] == requests.get(self.WINES_URL + str(new_wid)).json()['wine']:
                        key_ct = key_ct + 1
                        self.update(key_ct)
                    else:
                        new_wid = r['keys'][key_ct][0]
                        r = requests.get(self.WINES_URL + str(new_wid))
                        self.currentWine = r.json()
                        self.nameLabel.setText(self.currentWine['wine'])
                        self.info.setText("Country: " + self.currentWine["country"] + "\nWinery: "+ self.currentWine["winery"] 
                        + "\nVariety: " + self.currentWine["variety"] + "\nPoints (out of 100): " + self.currentWine['points'] + "\nDescription: " + self.currentWine['description'])
                        self.info.setVisible(True)
                        self.nameLabel.setVisible(True)
            except:
                self.nameLabel.setText("Invalid Taster name, try again")
                self.nameLabel.setVisible(True)

            

    def onRecommendPress(self):
        key_ct = 0
        self.update(key_ct)

    def updateTaster(self):
        self.tasterLabel.setText("Taster: " + self.tasterName)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    gui = MoviesQT()
    gui.show()

    sys.exit(app.exec_()) # starts the event queue
