#Group: pbaumann, jgarrard, jknutson, cnailos
#27 November 2018
#Wine Recommendation Server

import cherrypy
from _wine_database import _wine_database
from wine import WineController

@cherrypy.expose
def startService():
    wdb = _wine_database()
    f_path = 'data/winemag_2.txt'
    wdb.load_data(f_path)
    wineControlla = WineController(wdb)

    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    #Connect reset-DONE
    dispatcher.connect('reset', '/reset/', controller = wineControlla, action='RESET', conditions=dict(method=['PUT']))

    #Connect wine-DONE
    dispatcher.connect('replace_wine', '/wines/:key', controller = wineControlla, action='PUT_D', conditions=dict(method=['PUT']))
    dispatcher.connect('add_wine', '/wines/', controller = wineControlla, action='POST_D', conditions=dict(method=['POST']))
    dispatcher.connect('delete_wine', '/wines/:key', controller = wineControlla, action='DELETE_D', conditions=dict(method=['DELETE']))
    dispatcher.connect('get_wine', '/wines/:key', controller = wineControlla, action='GET_D', conditions=dict(method=['GET']))
    dispatcher.connect('get_wines', '/wines/', controller = wineControlla, action='GET_AD', conditions=dict(method=['GET']))

    #Delete all data
    dispatcher.connect('delete_all', '/wines/', controller = wineControlla, action='DELETE_A', conditions=dict(method=['DELETE']))

    dispatcher.connect('reset', '/reset/', controller = wineControlla, action='RESET', conditions=dict(method=['PUT']))

    #Connect taster
    dispatcher.connect('get_taster', '/tasters/:key', controller = wineControlla, action='GET_T', conditions=dict(method=['GET']))

    #Connect wineries
    dispatcher.connect('get_winery', '/wineries/:key', controller = wineControlla, action='GET_W', conditions=dict(method=['GET']))

    conf = {'global' : {'server.socket_host' : 'student04.cse.nd.edu', 'server.socket_port' : 52061}, '/' : {'request.dispatch' : dispatcher} }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

if __name__ == "__main__":
    startService()



