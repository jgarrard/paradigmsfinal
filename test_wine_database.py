from _wine_database import _wine_database
import unittest

class TestWineDatabase(unittest.TestCase):
    """unit tests for wine database class"""

    #@classmethod
    #def setUpClass(self):
    wdb = _wine_database()

    def reset_data(self):
        "reset data is required"
        self.wdb.clear_data()
        self.wdb.load_data('data/winemag_2.txt')

    def test_get_wine(self):
        self.reset_data()
        self.wdb.load_data('data/winemag_2.txt')
        wine = self.wdb.get_wine(21001)
        self.assertEquals(wine['country'], 'Chile')
        self.assertEquals(wine['vineyard'], 'Reserva Estate Bottled')

    def test_get_wine_null(self):
        self.reset_data()
        self.wdb.load_data('data/winemag_2.txt')
        wine = self.wdb.get_wine(-1)
        self.assertEquals(wine, None)

    def test_set_wine_taster(self): 
        self.reset_data()
        self.wdb.load_data('data/winemag_2.txt')
        wine = self.wdb.get_wine(21001)
        wine['taster'] = 'Phil'
        self.wdb.set_wine(21001, wine)
        self.assertEquals(self.wdb.tasters['Phil'], [21001])

    def test_set_wine(self):
        self.reset_data()
        self.wdb.load_data('data/winemag_2.txt')
        wine = self.wdb.get_wine(21001)
        wine['country'] = 'France'
        self.wdb.set_wine(21001, wine)
        wine = self.wdb.get_wine(21001)
        self.assertEquals(wine['country'], 'France')

    def test_set_wine_winery(self):
        self.reset_data()
        self.wdb.load_data('data/winemag_2.txt')
        wine = self.wdb.get_wine(21001)
        wine['winery'] = 'Phil'
        self.wdb.set_wine(21001, wine)
        self.assertEquals(self.wdb.wineries['Phil'], [21001])

    def test_delete_wine(self):
        self.reset_data()
        self.wdb.load_data('data/winemag_2.txt')
        self.wdb.delete_wine(21001)
        wine = self.wdb.get_wine(21001)
        self.assertEquals(wine, None)

    def test_get_wines(self):
        self.reset_data()
        self.wdb.load_data('data/winemag_2.txt')
        self.assertEquals(len(self.wdb.get_wines()),21000)

    def test_get_taster(self):
        self.reset_data()
        self.wdb.load_data('data/winemag_2.txt')
        taster = self.wdb.get_taster(21001)
        self.assertEquals(taster, 'Michael Schachner')

    def test_get_winery(self):
        self.reset_data()
        self.wdb.load_data('data/winemag_2.txt')
        winery = self.wdb.get_winery(21001)
        self.assertEquals(winery, 'Santa Alicia')

    def test_get_highest_rated_wine(self):
        self.reset_data()
        self.wdb.load_data('data/winemag_2.txt')
        wid = self.wdb.get_highest_rated_wine() # Confirmed that wid 36528 has highest rating with excel
        self.assertEquals(wid, 36528)

if __name__ == "__main__":
    unittest.main()
