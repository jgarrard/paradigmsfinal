import unittest
import requests
import json

class TestMovies(unittest.TestCase):

    SITE_URL = 'http://student04.cse.nd.edu:52061' # replace this with your port number
    WINES_URL = SITE_URL + '/wines/'
    TASTER_URL = SITE_URL + '/tasters/'
    WINERY_URL = SITE_URL + '/wineries/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, data = json.dumps(m))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp["result"], "success")

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_wines_get(self):
        self.reset_data()
        movie_id = 21001
        r = requests.get(self.WINES_URL + str(movie_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['taster'], 'Michael Schachner')

    def test_movies_put(self):
        self.reset_data()
        movie_id = 21001

        r = requests.get(self.WINES_URL + str(movie_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['taster'], 'Michael Schachner')

        resp['taster'] = "Justin Garrard"
        r = requests.put(self.WINES_URL + str(movie_id), data = json.dumps(resp))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.WINES_URL + str(movie_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['taster'], "Justin Garrard")

    def test_movies_delete(self):
        self.reset_data()
        movie_id = 21001

        m = {}
        r = requests.delete(self.WINES_URL + str(movie_id), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.WINES_URL + str(movie_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')

    def test_delete_reset(self):
        movie_id = 21001

        m = {}
        r = requests.delete(self.WINES_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.WINES_URL + str(movie_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')

        r = requests.put(self.RESET_URL)
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.WINES_URL + str(movie_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

    def test_tasters(self):
        self.reset_data()
        movie_id = 'Michael Schachner'
        m = {}
        r = requests.get(self.TASTER_URL + str(movie_id), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        print(resp["keys"])

    def test_wineries(self):
        self.reset_data()
        movie_id = "Santa Alicia"

        m = {}
        r = requests.get(self.WINERY_URL + str(movie_id), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        print(resp["keys"])

    def test_post(self):
        m = {"title" : "Winter Spice Blend", "country" : "USA", "winery":"West-Whitehill", "variety":"red", "taster":"Justin Garrard", "points":"85", "description":"very good warm"}
        r = requests.post(self.WINES_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.WINES_URL + str(resp["id"]))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['taster'], "Justin Garrard")


if __name__ == "__main__":
    unittest.main()

