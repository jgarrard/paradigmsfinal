# Group: pbaumann, jgarrard, jknutson, cnailos
# 5 October 2018
# Class for Wine Database
import csv

class _wine_database:

    def __init__(self):
        self.wine = dict()
        self.tasters = dict()
        self.wineries = dict()

    def load_data(self, data_file):
        f = open(data_file)
        self.wine = dict()
        self.tasters = dict()
        self.wineries = dict()
        for line in csv.reader(f, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL):
            self.wine[int(line[0])] = {
                    'country':str(line[1]),
                    'description':str(line[2]),
                    'vineyard':str(line[3]),
                    'points':line[4], # Cannot cast to data type because might be empty.
                    'price':line[5],  # Cannot cast to data type because might be empty.
                    'province':str(line[6]),
                    'region_1':str(line[7]),
                    'region_2':str(line[8]),
                    'taster':str(line[9]),
                    'title':str(line[11]),
                    'variety':str(line[12]),
                    'winery':str(line[13])
            }
            if str(line[9]) in self.tasters.keys():
                self.tasters[str(line[9])].append(int(line[0]))
            else:
                self.tasters[str(line[9])] = [int(line[0])]
            if str(line[13]) in self.wineries.keys():
                self.wineries[str(line[13])].append(int(line[0]))
            else:
                self.wineries[str(line[13])] = [int(line[0])]
        f.close()

    def clear_data(self):
        self.wine = dict()
        self.tasters = dict()
        self.wineries = dict()

    def print_wines(self):
        for wines in self.wine:
            print(self.wine[wines])

    def get_wine(self, wid):
        if wid in self.wine.keys(): return self.wine[wid]
        else: return None

    def get_wines(self):
        return list(self.wine.keys())

    def set_wine(self, wid, info): # Assumes correct info variable formatting matching key:value value formatting in wine dict.
        self.wine[int(wid)] = info
        if str(info['taster']) in self.tasters.keys():
            self.tasters[str(info['taster'])].append(int(wid))
        else:
            self.tasters[str(info['taster'])] = [int(wid)]
        if str(info['winery']) in self.wineries.keys():
            self.wineries[str(info['winery'])].append(int(wid))
        else:
            self.wineries[str(info['winery'])] = [int(wid)]
        return wid

    def delete_wine(self, wid):
        if wid in self.wine.keys():
            del self.wine[wid]
            for som in self.tasters.keys():
                if wid in self.tasters[som]:
                    self.tasters[som].remove(wid)
            for win in self.wineries.keys():
                if wid in self.wineries[win]:
                    self.wineries[win].remove(wid)

    def get_taster(self, som):
        wine_scores = {}
        for wid in self.tasters[som]:
            wine_scores[wid] = self.wine[wid]['points']
        return sorted(wine_scores.items(), key=lambda kv:kv[1])[:10]

    def get_winery(self, win):
        wine_scores = {}
        for wid in self.wineries[win]:
            wine_scores[wid] = self.wine[wid]['points']
        return sorted(wine_scores.items(), key=lambda kv:kv[1])[:10]

    def get_highest_rated_wine(self):
        highest_rated = 0
        highest_rated_wid = 0
        for wid in self.wine.keys():
            if self.wine[wid]['points']:
                if int(self.wine[wid]['points']) > highest_rated:
                    highest_rated_wid = wid
                    highest_rated = int(self.wine[wid]['points'])
        return highest_rated_wid

