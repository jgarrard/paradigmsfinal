#Group: pbaumann, jgarrard, jknutson, cnailos
#27 November 2018
#Wine Controller

import cherrypy
import json

class WineController(object):
    @cherrypy.expose
    def __init__(self, wdb):
        self.wine_db = wdb


    #DONE
    @cherrypy.expose
    def GET_D(self, key):
        res = self.wine_db.get_wine(int(key))
        if(res != None):
            dic = {"result" : "success", "key" : key, "wine" : res['title'], "country" : res["country"], "winery" : res["winery"], "variety" : res["variety"], "taster" : res["taster"], "points" : res["points"], "description" : res["description"]}
        else:
            dic = {"result" : "error", "key" : "key not found"}
        return json.dumps(dic)

    #DONE
    @cherrypy.expose
    def GET_AD(self):
        status = "success"
        res = self.wine_db.get_highest_rated_wine()
        return json.dumps({"result" : status, "wid" : res})

    #DONE
    @cherrypy.expose
    def PUT_D(self, key):
        data = json.loads(cherrypy.request.body.read())
        self.wine_db.set_wine(key, {"title" : data["wine"], "country" : data["country"], "winery" : data["winery"], "variety" : data["variety"], "taster" : data["taster"], "points" : data["points"], "description" : data["description"]})
        return json.dumps({"result" : "success"})

    #DONE
    @cherrypy.expose
    def POST_D(self):
        data = json.loads(cherrypy.request.body.read())
        wid = self.wine_db.set_wine(len(self.wine_db.get_wines())+1, {"title" : data["title"], "country" : data["country"], "winery" : data["winery"], "variety" : data["variety"], "taster" : data["taster"], "points" : data["points"], "description" : data["description"]})
        return json.dumps({"result" : "success", "id" : wid})

    #DONE
    @cherrypy.expose
    def DELETE_A(self):
        self.wine_db.clear_data()
        resp = {"result" : "success", "key" : "everything deleted"}
        return json.dumps(resp)

    #DONE
    @cherrypy.expose
    def DELETE_D(self, key):
        self.wine_db.delete_wine(int(key))
        resp = {"result" : "success", "key" : "key deleted"}
        return json.dumps(resp)

    #DONE
    @cherrypy.expose
    def GET_T(self, key):
        res = self.wine_db.get_taster(key)
        if(res != None):
            dic = {"result" : "success", "keys" : res}
        else:
            dic = {"result" : "error", "key" : "key not found"}
        return json.dumps(dic)

    #DONE
    @cherrypy.expose
    def GET_W(self, key):
        res = self.wine_db.get_winery(key)
        if(res != None):
            dic = {"result" : "success", "keys" : res}
        else:
            dic = {"result" : "error", "key" : "key not found"}
        return json.dumps(dic)

    def RESET(self):
        self.wine_db.load_data('data/winemag_2.txt')
        return json.dumps({"result" : "success", "key" : "data reset"})

